
import express from 'express';
import { exec } from 'child_process';
import stream from 'stream';
import https from 'https';

const app = express();
const port = 3000;

app.use((req, res) => {
  const videoUrl = `http:/${req.url}`
  if (!videoUrl) {
    return res.status(400).send('Missing video URL');
  }
  
  const ytDlpCommand = `yt-dlp -f bestaudio -g ${videoUrl} --no-check-certificates --user-agent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36" --cookies cookies.txt`;
  // Use yt-dlp to get the best audio stream URL
  exec(ytDlpCommand, (error, stdout, stderr) => {
    if (error) {
      console.error(`yt-dlp error: ${error}`);
      console.error(`stderr: ${stderr}`);
      return res.status(500).send('Error getting audio stream. YouTube may be blocking the request.');
    }

    const audioUrl = stdout.trim();

    // Set appropriate headers for audio streaming
    res.setHeader('Content-Type', 'audio/mpeg');
    res.setHeader('Transfer-Encoding', 'chunked');

    // Create a pass-through stream
    const pass = new stream.PassThrough();

    // Pipe the audio stream to the response
    https.get(audioUrl, {
      headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'
      }
    }, (audioRes) => {
      audioRes.pipe(pass);
    }).on('error', (err) => {
      console.error(`Error streaming audio: ${err}`);
      res.status(500).send('Error streaming audio');
    });

    pass.pipe(res);
  });
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});